// PingPongOS - PingPong Operating System
// Prof. Carlos A. Maziero, DAINF UTFPR
// Versão 1.0 -- Março de 2015
//
// Estruturas de dados internas do sistema operacional
#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>
#include <signal.h>
#include <sys/time.h>
#include <string.h>
#include <math.h>
#include "queue.h"

#define STACKSIZE 32768		/* tamanho de pilha das threads */
#define _XOPEN_SOURCE 600	/* para compilar no MacOS */

#ifndef __DATATYPES__
#define __DATATYPES__

enum type_t {User,System}; 
enum status {Suspense,Ready,Sleep,Finished,Semaforo}; 
// Estrutura que define uma tarefa
typedef struct task_t
{
    struct task_t *prev, *next ; //Ponteiros utilizados para a fila de tarefas
    int tid ;//Id da tarefa
    int prio,priod;//Prioridade estatica e dinamica
    struct task_t *parent;//tarefa da qual a tarefa sera chamada
    int t_init,t_end,t_execute,n_ativation;//Tempo de inicio, fim, de processador e numero de ativações da tarefa
    enum type_t type; //Tipo da tarefa , que pode ser de Usuario ou Sistema
    enum status stat;
    int waiting_queue,awake_time;
    int exit_code;
    ucontext_t context;// Contexto da tarefa
} task_t;

// estrutura que define um semáforo
typedef struct semaphore_t
{
  struct semaphore_t *prev, *next ;//Fila de semaforos principais
  int sid;
  int counter;
  task_t *task_queue;
  // preencher quando necessário
} semaphore_t ;

// estrutura que define um mutex
typedef struct
{
  // preencher quando necessário
} mutex_t ;

// estrutura que define uma barreira
typedef struct
{
  int bid;
  int n_tasks;
  int counter;
  semaphore_t b_sem;
} barrier_t ;

// estrutura que define uma fila de mensagens
typedef struct
{
  int qid;
  int tam_fila;
  int men_counter;
  char** fila_men;
  semaphore_t vaga_queue,men_queue;
  // preencher quando necessário
} mqueue_t ;

#endif
