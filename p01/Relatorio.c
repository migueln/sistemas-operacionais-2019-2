
Explicar:

1 - getcontext(&a)
	The function getcontext() initializes the structure pointed at by ucp to the currently active context.

	A função getcontext() inicializa o contexto ativo atual na variavel ucontext_t apontada.

	Paramêtros: ucontext_t *ucp



2 - setcontext(&a)
	The  function  setcontext() restores the user context pointed at by ucp.

	A função setcontext() ativa o contexto para o apontado pelo paramêtro.

 	Paramêtros: const ucontext_t *ucp
  

3 - swapcontext(&a,&b)
    The swapcontext() function saves the current context in the structure pointed to by oucp, and then activates the context pointed to by ucp.

    A função swapcontext() salva o contexto atual na variavel apontada pelo ponteiro oucp e ativa o contexto apontado pelo ucp.
	
	Paramêtros:(ucontext_t *oucp, const ucontext_t *ucp)

4 - makecontext(&a,...)	
	A função makecontext() modifica o contexto apontado pelo ponteiro ucp , contexto este obtido pela chamada da função getcontext. Antes de invocar o makecontext() deve-se alocar uma nova pilha para este contexto e designar o endereço de tal pilha para ucp->uc_stack e definir um contexto sucessor designando seu endereço para ucp->uc_link.
	Quando este contexto é ativado posteriormente a função func é chamada e recebe a sequencia de argumentos que seguem argc, sendo especificado o numero de tais argumentos em argc, quando essa função retorna seu contexo sucessor, ucp->uc_link, é ativado, se seu sucessor for nulo entao a thread sai deste contexto.

	Paramêtros: ucontext_t *ucp, void (*func)(), int argc, ...

5 - Explicar funções do ucontext :
      
      ucontext.uc_stack 
      Pilha usada para o contexto "ucontext".

      ucontext.uc_stack.ss_sp = stack ;
      Aponta para a base da pilha.

      ucontext.uc_stack.ss_size = STACKSIZE;
      Tamanho da pilha em bytes.

      ucontext.uc_stack.ss_flags;
      Uma flag que indica para o sistema se ele deve estabelecer uma nova pilha ou desabilitar uma pilha existente.
      ContextPing.uc_link = 0;
      uc_link -> Aponta para o contexto sucessor a ser executado quando o contexto atual der algum retorno.

6 - Explicar a dinamica do código, onde chamam as funções e as manipulações de ucontext:

7 Desenho do diagrama:
Ocupação do processador em escala de 5 µs:
			 mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm
ContextMain:|x x x x - - - - - - - - - - - - - - - - - - - - - - - - x - - x x|<-> Inicio: 22 clocks Fim:8 clocks Transição entre ping e pong fim = 2 clocks
ContextPing:|- - - - x x x x x - - - - - x x - - x x - - x x - - x x - - - - -|<-> Inicio: 14 clocks meio: 9 clocks/ciclo do for Fim:11 clocks
ContextPong:|- - - - - - - - - x x x x x - - x x - - x x - - x x - - - x x - -|<-> Inicio: 14 clocks meio: 9 clocks/ciclo do for Fim:11 clocks
			 mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm
