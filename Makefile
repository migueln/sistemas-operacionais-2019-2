p_00:
	cd p00 && gcc queue.h queue.c testafila.c -o testafila && ./testafila

p_01:
	cd p01 && gcc contexts.c -o contexts && ./contexts

p_02:
	cd p02 && gcc queue.h queue.c datatypes.h pingpong.h pingpong.c pingpong-tasks1.c -o task1 && ./task1 && gcc queue.h queue.c datatypes.h pingpong.h pingpong.c pingpong-tasks2.c -o task2 && ./task2 && gcc queue.h queue.c datatypes.h pingpong.h pingpong.c pingpong-tasks3.c -o task3 && ./task3

p_03:
	cd p03 && gcc queue.h queue.c datatypes.h pingpong.h pingpong.c pingpong-dispatcher.c -o dispatcher && ./dispatcher

p_04:
	cd p04 && gcc queue.h queue.c datatypes.h pingpong.h pingpong.c pingpong-scheduler.c -o scheduler && ./scheduler

p_05:
	cd p05 && gcc queue.h queue.c datatypes.h pingpong.h pingpong.c pingpong-preempcao.c -o preempcao && ./preempcao

p_06:
	cd p06 && gcc queue.h queue.c datatypes.h pingpong.h pingpong.c pingpong-contab-prio.c -o contab_prio && gcc queue.h queue.c datatypes.h pingpong.h pingpong.c pingpong-contab.c -o contab && ./contab &&./contab_prio

p_07:
	cd p07 && gcc queue.h queue.c datatypes.h pingpong.h pingpong.c pingpong-maintask.c -o maintask && ./maintask

p_08:
	cd p08 && gcc queue.h queue.c datatypes.h pingpong.h pingpong.c pingpong-join.c -o join && ./join

p_09:
	cd p09 && gcc queue.h queue.c datatypes.h pingpong.h pingpong.c pingpong-sleep.c -o sleep && ./sleep

p_10a:
	cd p10 && gcc queue.h queue.c datatypes.h pingpong.h pingpong.c pingpong-semaphore.c -o semaf && ./semaf

p_10b:
	cd p10 && gcc queue.h queue.c datatypes.h pingpong.h pingpong.c pingpong-racecond.c -o race && ./race

p_10c:
	cd p10 && gcc queue.h queue.c datatypes.h pingpong.h pingpong.c pingpong-prodcons.c -o prodcons && ./prodcons

p_11:
	cd p11 gcc queue.h queue.c datatypes.h pingpong.h pingpong.c pingpong-barrier.c -o barrier && ./barrier

p_12:
	cd p12 gcc queue.h queue.c datatypes.h pingpong.h pingpong.c pingpong-mqueue.c -o mqueue -lm && ./mqueue

p_13:
	cd p13 gcc queue.h queue.c datatypes.h diskdriver.h harddisk.h harddisk.c  pingpong.h pingpong.c pingpong-disco.c -o disco -lm -lrt && ./disco

p_14:
	cd p14 gcc queue.h queue.c datatypes.h diskdriver.h harddisk.h harddisk.c  pingpong.h pingpong.c pingpong-disco.c -o disco -lm -lrt && ./disco

