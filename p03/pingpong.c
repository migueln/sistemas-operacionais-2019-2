
#include "pingpong.h"
char *stack ;
int static n_task,n_abs_tasks; // Numero de tarefas e numero de tarefas ativas

task_t t_main,t_dispatcher,*task_on,*t_ready,*t_suspend;//Tarefa Main, tarefa despachadora, ponteiro para tarefa atual, fila de tarefas prontas, fila de tarefas suspensas

ucontext_t c_main;// Contexto da taerfa Main
//Função que retorna a proxima task a ser chamada,modelo fifo
task_t *scheduler()
{	task_t *next=t_ready;
	t_ready=t_ready->next;//Atualiza o ponteiro da fila 
	return(next);// retorna o primeiro da fila
}
void dispatcher_body () // dispatcher é uma tarefa
{	
	task_t *next;
	while ( n_abs_tasks > 1 )
	{
		next = scheduler() ; // scheduler é uma função
		if (next)
		{
			task_switch (next) ; // transfere controle para a tarefa "next"
		}
	}
 task_exit(0) ; // encerra a tarefa dispatcher
}



void pingpong_init (){
//Iniciação de ponteiros
    task_on = NULL;
    t_ready = NULL;
    t_suspend = NULL;

//Iniciação das estaticas
    n_abs_tasks=0;
    n_task=0;

    setvbuf (stdout, 0, _IONBF, 0) ;

//Iniciação da tarefa Main
    t_main.next=&t_main;
    t_main.prev=&t_main;
    t_main.tid=n_task;
    t_main.context=c_main;
    task_on=&t_main;

//Criação da tarefa despachadora
    task_create(&t_dispatcher,dispatcher_body,0);

}

int task_create (task_t *task,void (*start_func)(void *),void *arg){
//Aumenta o numero de tarefas e de tarefas prontas
	n_abs_tasks++;
    n_task++;
//Aloca pilha e contexto da tarefa
    stack = malloc (STACKSIZE) ;
	getcontext (&(task->context));
    task->context.uc_stack.ss_sp = stack ;
    task->context.uc_stack.ss_size = STACKSIZE;
    task->context.uc_stack.ss_flags = 0;
    task->context.uc_link = 0;
    task->tid=n_task;

//Decide se a tarefa é chamada pela despachadora ou diretamente pela main
    if(n_task==1)
    	task->parent=&t_main;
    else
    	task->parent=&t_dispatcher;

//Atribui uma função para esta tarefa
    makecontext (&(task->context), (void*)(*start_func), 1,arg);

//Coloca a tarefa na fila de prontos
  	if(n_task>1)
	  	queue_append ((queue_t **) &t_ready,(queue_t*) task);
//Retorna id da tarefa
    return(n_task);
}

int task_switch (task_t *task) {
	ucontext_t *aux;
//Pega o contexto da tarefa atual e realiza a troca de contextos, atualizando a tarefa atual para a tarefa passada como parametro
	aux=&(task_on->context);
	task_on=task;
    swapcontext ((aux), &(task->context)) ;
	return (task->tid);
}

void task_exit (int exitCode){
//Decrementa o numero de tarefas prontas ,tira a tarefa da fila de prontos e volta para a tarefa do qual ela foi chamada
	n_abs_tasks--;
	if(task_on->parent==&t_dispatcher)
	queue_remove ((queue_t**) &t_ready, (queue_t*) task_on) ;
   	task_switch (task_on->parent) ;

}

void task_yield (){
	//Retorna o controle para a tarefa despachadora
	task_switch(&t_dispatcher);
}

// retorna o id da tarefa
int task_id () {
	return(task_on->tid);
}


//Suspende a tarefa a tirando da fila dos prontos e a colocando na fila das suspensas
void task_suspend (task_t *task, task_t **queue){
	if(queue==NULL)
		return;
	else{
		if (task==NULL)
			task=task_on;

		n_abs_tasks--;

		if(task->parent==&t_dispatcher)
		queue_remove ((queue_t**) &t_ready, (queue_t*) task) ;

	  	queue_append ((queue_t **) queue,(queue_t*) task);

	  	if(task_on==task)
	   	task_switch (task_on->parent) ;


	}
}
//Retira a tarefa da fila das tarefas suspensas e a coloca na fila das tarefas prontas denovo.
void task_resume (task_t *task){
	queue_remove ((queue_t**) &t_suspend, (queue_t*) task);
  	queue_append ((queue_t **) &t_ready,(queue_t*) task);

}
