#include "queue.h"
#include <stdio.h>
//Adiciona um elemento no fim da fila e configura os ponteiros dos elementos anteriores e posteriores ao adicionado
void queue_append (queue_t **queue, queue_t *elem){
	if(elem->next!=NULL)
	{
		printf("O elemento ja esta em outra fila.\n");
	}
	else if(*queue==NULL)
	{ 
		(*queue)=elem;
		(*queue)->prev=(*queue);
		(*queue)->next=(*queue);

	}

	else 
	{
		queue_t *aux;
		aux=*queue;
		while(aux->next!=(*queue))
		{

			aux=aux->next;	
		}
		aux->next=elem;
		elem->prev=aux;
		elem->next=(*queue);
		(*queue)->prev=elem;

	}
}
//Tira o elemento passado pelo parametro da fila

queue_t *queue_remove (queue_t **queue, queue_t *elem){
	if(*queue==NULL)//Se a fila esta vazia retorna erro
	{
			printf("Fila Vazia\n");
			return NULL;
	}
	else if((*queue)->next==(*queue) && (*queue)==elem){
		*queue=NULL;
		elem->prev=NULL;
		elem->next=NULL;
		return elem;
	}
	else
	{	
		int i;
		i=0;
		queue_t *aux;
		aux=*queue;
		while (aux!=elem)
		{	

			if(aux->next==(*queue))
			{
				printf("Elemento nao pertence a fila indicada\n");
				return NULL;
			}
			aux=aux->next;
		}


		aux->prev->next=aux->next;
		aux->next->prev=aux->prev;

		if(*queue==aux)
			*queue=aux->next;

		aux->prev=NULL;
		aux->next=NULL;
		
		return elem;
	}
}

//Retorna o tamanho da fila
int queue_size (queue_t *queue){
	if(queue==NULL)
	{
		return 0;
	}
	else
	{
		queue_t *aux,*aux2;
		aux=queue;
		aux2=queue;

		int i=0;
		while (aux->next!=aux2)
		{
			aux=aux->next;
			i++;
		}
		i++;
		return i;
	}
}

//Printa os elementos da fila atravez de uma função passada como parametro
void queue_print (char *name, queue_t *queue, void print_elem (void*) ){
	printf("%s: ",name );
	if(queue==NULL)
	{
		printf("[]\n");
		return;
	}
	else
	{
		queue_t *aux,*aux2;
		aux=queue;
		aux2=queue;
		int i=0;
		printf("[");
		while (aux->next!=aux2)
		{
			print_elem(aux);
			printf(" ");
			aux=aux->next;

		}
		print_elem(aux);
		printf("]\n");
	}
}
