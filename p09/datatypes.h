// PingPongOS - PingPong Operating System
// Prof. Carlos A. Maziero, DAINF UTFPR
// Versão 1.0 -- Março de 2015
//
// Estruturas de dados internas do sistema operacional
#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>
#include <signal.h>
#include <sys/time.h>
#include "queue.h"

#define STACKSIZE 32768		/* tamanho de pilha das threads */
#define _XOPEN_SOURCE 600	/* para compilar no MacOS */

#ifndef __DATATYPES__
#define __DATATYPES__

enum type_t {User,System}; 
enum status {Suspense,Ready,Sleep,Finished}; 
// Estrutura que define uma tarefa
typedef struct task_t
{
    struct task_t *prev, *next ; //Ponteiros utilizados para a fila de tarefas
    int tid ;//Id da tarefa
    int prio,priod;//Prioridade estatica e dinamica
    struct task_t *parent;//tarefa da qual a tarefa sera chamada
    int t_init,t_end,t_execute,n_ativation;//Tempo de inicio, fim, de processador e numero de ativações da tarefa
    enum type_t type; //Tipo da tarefa , que pode ser de Usuario ou Sistema
    enum status stat;
    int waiting_queue,awake_time;
    int exit_code;
    ucontext_t context;// Contexto da tarefa
} task_t;

// estrutura que define um semáforo
typedef struct
{
  // preencher quando necessário
} semaphore_t ;

// estrutura que define um mutex
typedef struct
{
  // preencher quando necessário
} mutex_t ;

// estrutura que define uma barreira
typedef struct
{
  // preencher quando necessário
} barrier_t ;

// estrutura que define uma fila de mensagens
typedef struct
{
  // preencher quando necessário
} mqueue_t ;

#endif
