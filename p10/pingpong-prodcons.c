
#include <stdio.h>
#include <stdlib.h>
#include "pingpong.h"

// operating system check
#if defined(_WIN32) || (!defined(__unix__) && !defined(__unix) && (!defined(__APPLE__) || !defined(__MACH__)))
#warning Este codigo foi planejado para ambientes UNIX (LInux, *BSD, MacOS). A compilacao e execucao em outros ambientes e responsabilidade do usuario.
#endif


#define PRODUCAO 100

semaphore_t s_buffer , s_item , s_vaga;
task_t prod1,prod2,prod3,cons1,cons2;
int buffer[5]={-1,-1,-1,-1,-1};
int produzido=0;
int consumido=0;

void insere_buffer(int item)
{
    
    int i,lotado;
    i=0;
    lotado=0;
    while(i<5){
        if (buffer[i]==-1)
            {break;}
        i++;
    }
    if (i==4 && buffer[i]!=-1)
        {
            printf("Erro lotado\n");
            exit(0);
        }
    buffer[i]=item;
}

int remove_buffer()
{
    int i,item_tirado;
    i=0;
    if (buffer[0]==-1)
        {
            printf("ERRO buffer vazio\n");
            exit(0);
        }
    item_tirado=buffer[0];

    while(i<4){
        buffer[i]=buffer[i+1];
        i++;
    }
    buffer[i]=-1;
    return item_tirado;
}

void produtor(void* id)
{
    printf("Começou %s\n",(char*)id);
    int item;
    while (produzido<PRODUCAO)
    {
        produzido++;

        task_sleep (1);
        item = random ()%100;
        sem_down (&s_vaga);
        sem_down (&s_buffer);

        insere_buffer(item);

        printf("%s produziu %d\n",(char*)id,item);
        sem_up (&s_buffer);
        sem_up (&s_item);
    }

    task_exit (0) ;

}

void consumidor(void* id)
{
    printf("Começou %s\n",(char*)id);
    int removido;
    while (consumido<PRODUCAO)
    {
        sem_down (&s_item);
        sem_down (&s_buffer);

        removido=remove_buffer();
        consumido++;
        sem_up (&s_buffer);
        sem_up (&s_vaga);
        printf("\t%s consumiu %d\n",(char*)id,removido);

        task_sleep (1);
    }
    task_exit (0) ;

}   

int main (int argc, char *argv[])
{   
    printf("Main inicio\n");
    pingpong_init () ;

    sem_create (&s_buffer, 1) ;
    sem_create (&s_item, 0) ;
    sem_create (&s_vaga, 4) ;

    task_create (&prod1, produtor, "p1") ;
    task_create (&prod2, produtor, "p2") ;
    task_create (&prod3, produtor, "p3") ;
    task_create (&cons1, consumidor, "c1") ;
    task_create (&cons2, consumidor, "c2") ;
    printf("Criou tasks e semaforos\n");

    printf("Começou tasks\n");
    task_join (&prod1) ;
    task_join (&prod2) ;
    task_join (&prod3) ;
    task_join (&cons1) ;
    task_join (&cons2) ;

    sem_destroy (&s_buffer) ;
    sem_destroy (&s_item) ;
    sem_destroy (&s_vaga) ;

    printf("Produzido devia ser 100 e foi %d\n",produzido);
    printf("consumido devia ser 100 e foi %d\n",consumido);
    task_exit (0) ;

   exit (0) ;
}