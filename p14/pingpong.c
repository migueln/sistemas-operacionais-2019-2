
#include "pingpong.h"
#include "diskdriver.h"
#include "harddisk.h"
#define TIME_Q 20//Tamanho do quantum
#define FCFS 0
#define SSTF 1
#define CSCAN 2
#define ESCALONADOR SSTF

char *stack ;
int static n_task,n_abs_tasks,n_twaiting,quantum,time_global; //Numero de tarefas, numero de tarefas prontas, tempo que sera atribuido a cada tarefa(de usuario) antes da interrupção e tempo global
int static atomicity,sem_count,bar_count,queue_count,sig_flag,cab_disk,count_blocks;

task_t t_main,t_dispatcher,t_disk_manager,*task_on,*t_ready,*t_suspend,*t_sleep;//Tarefa main, tarefa dispachadora, ponteiro para a tarefa atual, ponteiro para fila de tarefas prontas e ponteiro para fila de suspensas 
semaphore_t *sem_ready;
disk_t disk;
ucontext_t c_main;//Contexto da tarefa main

struct sigaction action ;//Estrutura de sinal
struct sigaction action_disk ;//Estrutura de sinal
struct itimerval timer;//Estrutura do timer


void tratador();// Função que trata os sinais recebidos, nesse caso "ticks"
void tratador_disk();// Função que trata os sinais recebidos, nesse caso "flags"
pedidos_t* disk_esc();

//Função que retorna o tempo global atual do programa
unsigned int systime (){
	return(time_global);
}

//Retorna a proxima função chamada, Modelo de prioridade
task_t *scheduler(){	
	task_t *next,*aux;
	aux=t_ready->next;
	next=t_ready;
	while(aux!=t_ready)
	{	
		//Decide a tarefa com menor prioridade para ser chamada
		if(next->priod>aux->priod)
			next=aux;
		aux=aux->next;

	}
	aux=t_ready;
	while(aux->next!=t_ready)
	{
		//Envelhece as tarefas
		aux->priod=aux->priod-1;
		aux=aux->next;
	}
	aux->priod=aux->priod-1;

	next->priod=next->prio;


	return(next);
}

void dispatcher_body () // dispatcher é uma tarefa que decide a ordem e chama as tarefas de usuario
{	


	task_t *next;
	while ( !(n_abs_tasks < 1 && t_sleep==NULL && n_twaiting<1))
	{
		if(t_sleep!=NULL)
		{
			task_t *aux,*next;
			aux=t_sleep;
			while(aux->next!=t_sleep)
			{	
				next=aux->next;
				if(aux->awake_time<=systime())
					task_resume(aux);
				aux=next;
			}
			if(aux->awake_time<=systime())
				task_resume(aux);
		}

		if (n_abs_tasks < 1)
			continue;
		next = scheduler() ;  // scheduler é uma função que decidide qual sera a proxima tarefa de usuario chamada
		if (next)
		{
			task_switch(next) ; // transfere controle para a tarefa "next"
		}

		if(n_abs_tasks==1 && n_twaiting==1 && t_suspend->tid==t_disk_manager.tid)
			{
				printf("\n\nO número de blocos percorridos foi %d\n",count_blocks);
				printf("O tempo processador da task que executa as medidas de escalonamento é %d ms\n\n",t_disk_manager.t_execute);
				break;
			}
	}
 task_exit(0) ; // encerra a tarefa dispatcher
}




void pingpong_init (){
    setvbuf (stdout, 0, _IONBF, 0) ;
//Iniciação das variaves estaticas
	time_global=0;
    n_task=0;
    n_abs_tasks=0;
	sem_count=0;
	bar_count=0;
	n_twaiting=0;
	queue_count=0;
	sig_flag=0;
//Iniciação dos ponteiros e filas
    t_ready = NULL;
    task_on = NULL;
    t_suspend = NULL;
	sem_ready = NULL;

//Iniciação da tarefa main
    t_main.prio=0;
    t_main.tid=n_task;
	getcontext (&(t_main.context));
    t_main.type=System;
    t_main.t_init=0;
    t_main.t_end=0;
    t_main.t_execute=0;
    t_main.n_ativation=1;
	t_main.stat=Ready;
	t_main.waiting_queue=-1;
	t_main.awake_time=-1;
    task_on=&t_main;
	t_main.p_task.id_pai=t_main.tid;
	queue_append ((queue_t **) &t_ready,(queue_t*) &t_main);

//Criacao da tarefa despachadora
    task_create(&t_dispatcher,dispatcher_body,0);
    t_dispatcher.type=System;
	t_main.parent=&t_dispatcher;

//Declarações para captar o sinal
	action.sa_handler = tratador ;
	sigemptyset (&action.sa_mask) ;
	action.sa_flags = 0 ;
	if (sigaction (SIGALRM, &action, 0) < 0)
	{
	perror ("Erro em sigaction: ") ;
	exit (1) ;
	}

	action_disk.sa_handler = tratador_disk ;
	sigemptyset (&action_disk.sa_mask) ;
	action_disk.sa_flags = 0 ;
	if (sigaction (SIGUSR1, &action_disk, 0) < 0)
	{
	perror ("Erro em action_disk: ") ;
	exit (1) ;
	}

	// ajusta valores do temporizador
	timer.it_value.tv_usec = 1000 ;      // primeiro disparo, em micro-segundos
	timer.it_value.tv_sec  = 0 ;      // primeiro disparo, em segundos
	timer.it_interval.tv_usec = 1000 ;   // disparos subsequentes, em micro-segundos
	timer.it_interval.tv_sec  = 0 ;   // disparos subsequentes, em segundos
	quantum=0;
	// arma o temporizador ITIMER_REAL (vide man setitimer)
	if (setitimer (ITIMER_REAL, &timer, 0) < 0)
	{
	perror ("Erro em setitimer: ") ;
	exit (1) ;
	}
}



int task_create (task_t *task,void (*start_func)(void *),void *arg){
//Incrementa tarefa na fila de prontos e o numero de tarefas
	n_abs_tasks++;
    n_task++;



//Aloca valores de contexto e função utilizada pela tarefa
    stack = malloc (STACKSIZE) ;
	getcontext (&(task->context));
    task->context.uc_stack.ss_sp = stack ;
    task->context.uc_stack.ss_size = STACKSIZE;
    task->context.uc_stack.ss_flags = 0;
    task->context.uc_link = 0;
    task->tid=n_task;
	task->stat=Ready;
    task->type=User;
	task->waiting_queue=-1;
	task->awake_time=-1;
	task->p_task.id_pai=task->tid;
	task->p_task.prev=NULL;
	task->p_task.next=NULL;
	task->p_task.block=-1;
	task->p_task.ped_type=-1;
	task->p_task.buffer=NULL;
//Aloca a prioridade padrao,0.
    task->prio=0;

//Inicia timer da tarefa
    task->t_init=systime();
    task->t_execute=0;
    task->t_end=0;
    task->n_ativation=0;

  //Decide por quem a tarefa sera chamada
    if(n_task==1)
    	task->parent=&t_main;
    else
    	task->parent=&t_dispatcher;

    makecontext (&(task->context), (void*)(*start_func), 1,arg);
  	if(n_task>1)
	  	queue_append ((queue_t **) &t_ready,(queue_t*) task);

    return(n_task);

}

//Troca o controle da tarefa atual para a tarefa passada como parametro
int task_switch (task_t *task) {
	quantum=TIME_Q;
	ucontext_t *aux;
	aux=&(task_on->context);
	task_on=task;
	task->n_ativation=task->n_ativation+1;
    swapcontext ((aux), &(task->context)) ;
	return (task->tid);
}

//Termina a tarefa, gravando o tempo global no qual a mesma termina
void task_exit (int exitCode){
	n_abs_tasks--;
	task_on->t_end=systime();
	task_on->exit_code=exitCode;
	printf("Task %d exit: execution time %d ms, processor time %d ms, %d activations\n",task_on->tid,(task_on->t_end-task_on->t_init),task_on->t_execute,task_on->n_ativation);

	if (task_on->tid!=1)
		queue_remove ((queue_t**) &t_ready, (queue_t*) task_on) ;

	if(t_suspend!=NULL)
	{
		task_t *aux,*next;
		aux=t_suspend;
		while(aux->next!=t_suspend)
		{	
			next=aux->next;
			if(aux->waiting_queue==task_on->tid)
				task_resume(aux);
			aux=next;
		}
		if(aux->waiting_queue==task_on->tid)
			task_resume(aux);
	}
	task_on->stat=Finished;
	task_switch(task_on->parent) ;

}

// Reseta timer e devolve o controle para a tarefa despachadora
void task_yield (){
	quantum=TIME_Q;
	task_switch(&t_dispatcher);
}

// Retorna id da tarefa
int task_id () {
	return(task_on->tid);
}


//Suspende a tarefa e a coloca na fila passada no parametro
void task_suspend (task_t *task, task_t **queue){

	if (task==NULL)
		task=task_on;

	n_twaiting++;
	n_abs_tasks--;
	queue_remove ((queue_t**) &t_ready, (queue_t*) task) ;

	queue_append ((queue_t **) queue,(queue_t*) task);

	task->stat=Suspense;


	
}

//Acorda a tarefa passada pelo parametro e a coloca na fila de prontas
void task_resume (task_t *task){

	if(task==NULL)
		return;
	n_twaiting--;
	n_abs_tasks++;
	if(task->stat==Semaforo){

		semaphore_t *aux,*next;
		next=sem_ready;
		aux=sem_ready->next;
		while(aux!=sem_ready && aux->sid!=task->waiting_queue)
			aux=aux->next;
		if(aux->sid==task->waiting_queue)
		{
		queue_remove ((queue_t**) &(aux->task_queue), (queue_t*) task);

		}
		else
		{
			printf("ERRO NAO ACHOU A TASK NA FILA \n");
			exit(-1);
		}

	}
	else if(task->stat==Suspense)
		queue_remove ((queue_t**) &t_suspend, (queue_t*) task);
 	else if(task->stat==Sleep)
		queue_remove ((queue_t**) &t_sleep, (queue_t*) task);
	else 
	{	printf("ERROOOOOOO\n");
		   exit (-1) ;
	} 
  	queue_append ((queue_t **) &t_ready,(queue_t*) task);
	task->stat=Ready;
	task->waiting_queue=-1;
	task->prio=0;
}


//Suspende a tarefa atual até que a tarefa designada termine seu processo
int task_join (task_t *task) 
{
	if (task!=NULL && task->stat==Ready )
	{	
		task_suspend(NULL,&t_suspend);
		task_on->waiting_queue=task->tid;
		task_switch(task);
		return (task->exit_code);
	}
	else if (task!=NULL && task->stat==Suspense )
	{	
		task_suspend(NULL,&t_suspend);
		task_on->waiting_queue=task->tid;
		task_switch(&t_dispatcher);
		return (task->exit_code);
	}
	else if (task!=NULL && task->stat==Sleep )
	{	
		task_suspend(NULL,&t_suspend);
		task_on->waiting_queue=task->tid;
		task_switch(&t_dispatcher);
		return (task->exit_code);
	}
	else
		return -1;
}

// suspende a tarefa corrente por t segundos
void task_sleep (int t)
{	
	if (t>0)
	{
		int time=systime();
		task_suspend(NULL,&t_sleep);
		task_on->stat=Sleep;
		task_on->awake_time=time+t*1000;
		task_switch(task_on->parent);
	}
}




// define a prioridade estática de uma tarefa (ou a tarefa atual)
void task_setprio (task_t *task, int prio){
	if(task==NULL)
		task=task_on;
	task->prio=prio;
	if (task->prio>20)
		task->prio=20;
	if(task->prio<-20)
		task->prio=-20;

	task->priod=task->prio;

}

// retorna a prioridade estática de uma tarefa (ou a tarefa atual)
int task_getprio (task_t *task){
	if (task==NULL)
		task=task_on;
	return(task->prio);

}

//Trata o sinal recebido, Decrementando o quantum e incrementando o tempo de processador da tarefa usuario atual, e eventualmente devolvendo o controle para a despachadora
void tratador (int signum)
{


	time_global=time_global+1;
	task_on->t_execute++;

	quantum=quantum-1;

	if (task_on->tid==1)
		quantum=TIME_Q;
	else if (quantum==0)
		task_yield();

}


// cria um semáforo com valor inicial "value"
int sem_create (semaphore_t *s, int value){
	atomicity=1;
	s->sid=sem_count;
    s->counter=value;
    s->task_queue=NULL;
    s->next=NULL;
	s->prev=NULL;
	queue_append ((queue_t **) &sem_ready,(queue_t*) s);
	sem_count++;
	atomicity=0;
    return 0;

}

// requisita o semáforo
int sem_down (semaphore_t *s){
	atomicity=1;
	s->counter--;
    if (s->counter < 0){
        task_suspend(task_on, &(s->task_queue));
		task_on->stat=Semaforo;
		task_on->waiting_queue=s->sid;
        task_switch(&t_dispatcher);
		atomicity=0;
    return 0;
	}
	atomicity=0;
}

// libera o semáforo
int sem_up (semaphore_t *s) {

	atomicity=1;
	s->counter++;
    if (s->counter<=0){
        task_resume(s->task_queue);
	}
	atomicity=0;
	return 0;
}

// destroi o semáforo, liberando as tarefas bloqueadas
int sem_destroy (semaphore_t *s) {
	atomicity=1;
	if (s->task_queue!=NULL)
	{
		task_t *aux,*next;
		aux=s->task_queue;
		while(aux->next!=s->task_queue)
		{	
			next=aux->next;
			task_resume(aux);
			aux=next;
		}
		task_resume(aux);
	}
	queue_remove ((queue_t**) &sem_ready, (queue_t*) s) ;
	s->sid=-1;
	atomicity=0;
	return 0;
}



// Inicializa uma barreira
int barrier_create (barrier_t *b, int N)
{	
	b->bid=bar_count;
	b->n_tasks=N;
	b->counter=0;
	sem_create(&(b->b_sem),0);
	bar_count++;
	return 0;
}
// Chega a uma barreira
int barrier_join (barrier_t *b) 
{
	b->counter++;
	if(b->counter==b->n_tasks)
	{
		b->counter=0;
		sem_destroy(&(b->b_sem));
		sem_create((&b->b_sem),0);
	}
	else
	{
		sem_down(&(b->b_sem));
	}
	return 0;
}

// Destrói uma barreira
int barrier_destroy (barrier_t *b)
{
	b->bid=-1;
	b->n_tasks=-1;
	b->counter=-1;
	sem_destroy(&(b->b_sem));
	return 0;
}


// filas de mensagens

// cria uma fila para até max mensagens de size bytes cada
int mqueue_create (mqueue_t *queue, int max, int size)
{
	queue->qid=queue_count;
	queue->men_counter=0;
	queue->tam_fila=max;
	queue_count++;

	queue->fila_men = (char **)malloc(max * sizeof(char*));
	for(int i = 0; i < max; i++) 
	{	
		queue->fila_men[i] = (char *)malloc(size * sizeof(char));
		strcpy(queue->fila_men[i],"");
	}
	sem_create(&(queue->vaga_queue),max-1);
	sem_create(&(queue->men_queue),0);
	return 0;
}

// envia uma mensagem para a fila
int mqueue_send (mqueue_t *queue, void *msg) 
{
	if(queue->qid==-1)
		return -1;

	sem_down(&(queue->vaga_queue));
	if(queue->vaga_queue.sid==-1 || queue->qid==-1)
		task_exit(-1);
	
	strcpy(queue->fila_men[queue->men_counter],(char*) msg);
	sem_up(&(queue->men_queue));
	queue->men_counter++;
	return 0;
}

// recebe uma mensagem da fila
int mqueue_recv (mqueue_t *queue, void *msg) 
{
	if(queue->qid==-1)
		return -1;

	sem_down(&(queue->men_queue));
	if(queue->men_queue.sid==-1 || queue->qid==-1)
		task_exit(-1);
	strcpy(msg,queue->fila_men[0]);

	int aux=0;
	while(aux<(queue->men_counter-1))
	{
		strcpy(queue->fila_men[aux],queue->fila_men[aux+1]);
		aux++;
	}
	strcpy(queue->fila_men[aux],"");
	queue->men_counter--;
	sem_up(&(queue->vaga_queue));
	return 0;
}
// destroi a fila, liberando as tarefas bloqueadas
int mqueue_destroy (mqueue_t *queue) 
{
  queue->qid=-1;
  queue->tam_fila=-1;
  queue->men_counter=-1;
  free(queue->fila_men);
  sem_destroy(&(queue->vaga_queue));
  sem_destroy(&(queue->men_queue));
  return 0;
}
// informa o número de mensagens atualmente na fila
int mqueue_msgs (mqueue_t *queue)
{
	return(queue->men_counter);
}



// TASK DE GERENCIAMENTO DE DISCO
void diskDriverBody (void * args)
{
	while (1)
	{
		int x;

		// obtém o semáforo de acesso ao disco
		sem_down(&disk.s_disk);
		// se foi acordado devido a um sinal do disco
		if (sig_flag==1)
		{
			task_resume(disk.task_atendida);
			disk.task_atendida=NULL;
			sig_flag=0;
		// acorda a tarefa cujo pedido foi atendido
		}

		// se o disco estiver livre e houver pedidos de E/S na fila
		if (disk_cmd (DISK_CMD_STATUS, 0, 0) == DISK_STATUS_IDLE && disk.p_queue!=NULL)
		{
	//		printf("Chegou 1\n");
			pedidos_t *pedido_atual;
			pedido_atual=disk_esc();
			if(pedido_atual->ped_type==DISK_STATUS_READ)
			{
				disk_cmd (DISK_CMD_READ, pedido_atual->block, pedido_atual->buffer) ;
			//	printf("Chegou 2\n");

			}
			else if(pedido_atual->ped_type==DISK_STATUS_WRITE)
			{
				disk_cmd (DISK_CMD_WRITE, pedido_atual->block, pedido_atual->buffer) ;

			}
			task_t *aux,*task_aten;

			aux=t_suspend;
			while(aux->next!=t_suspend)
			{
				if(pedido_atual->id_pai==aux->tid)
					{
						task_aten=aux;
							break;
					}
				aux=aux->next;
			}
			if(pedido_atual->id_pai==aux->tid)
				task_aten=aux;

			disk.task_atendida=task_aten;


			queue_remove ((queue_t**) &disk.p_queue, (queue_t*) pedido_atual) ;

		// escolhe na fila o pedido a ser atendido, usando FCFS
		// solicita ao disco a operação de E/S, usando disk_cmd()
		}
		// libera o semáforo de acesso ao disco
		sem_up(&disk.s_disk);
		// suspende a tarefa corrente (retorna ao dispatcher)
		task_suspend(NULL,&t_suspend);
		task_switch(&t_dispatcher);
	}
}

// inicializacao do driver de disco
// retorna -1 em erro ou 0 em sucesso
// numBlocks: tamanho do disco, em blocos
// blockSize: tamanho de cada bloco do disco, em bytes
int diskdriver_init (int *numBlocks, int *blockSize)
{
	/*
	printf("Que metodo de escalonacao deseja?\n");
	printf("0 -> First Come, First Served (FCFS)\n");
	printf("1 -> Shortest Seek-Time First (SSTF)\n");
	printf("2 -> Circular Scan (CSCAN)\n");
	*/
	disk.task_atendida=NULL;
	disk.p_queue=NULL;
	sem_create(&disk.s_disk,1);
	disk_cmd(DISK_CMD_INIT, 0, 0);
    task_create(&t_disk_manager,diskDriverBody,0);
	task_suspend(&t_disk_manager, &t_suspend);

	cab_disk=0;
	count_blocks=0;
	*numBlocks=disk_cmd(DISK_CMD_DISKSIZE,0,0);
	*blockSize=disk_cmd(DISK_CMD_BLOCKSIZE,0,0);
	return 0;
}

// leitura de um bloco, do disco para o buffer indicado
int disk_block_read (int block, void *buffer)
{
	// obtém o semáforo de acesso ao disco
	sem_down(&disk.s_disk);
	// inclui o pedido na fila de pedidos de acesso ao disco
	task_on->p_task.block=block;
	task_on->p_task.buffer=buffer;
	task_on->p_task.ped_type=DISK_STATUS_READ;

	queue_append ((queue_t **) &disk.p_queue,(queue_t*) &task_on->p_task);
	if (t_disk_manager.stat==Suspense)
	{
		task_resume(&t_disk_manager);
		// acorda o gerente de disco (põe na fila de prontas)
	}
	// libera semáforo de acesso ao disco
	sem_up(&disk.s_disk);
	// suspende a tarefa corrente (retorna ao dispatcher)
	task_suspend(task_on, &t_suspend);
	task_switch(&t_dispatcher);
	return 0;
}
// escrita de um bloco, do buffer indicado para o disco
int disk_block_write (int block, void *buffer) 
{
	// obtém o semáforo de acesso ao disco
	sem_down(&disk.s_disk);
	// inclui o pedido na fila de pedidos de acesso ao disco
	task_on->p_task.block=block;
	task_on->p_task.buffer=buffer;
	task_on->p_task.ped_type=DISK_STATUS_WRITE;

	queue_append ((queue_t **) &disk.p_queue,(queue_t*) &task_on->p_task);
	if (t_disk_manager.stat==Suspense)
	{
		task_resume(&t_disk_manager);
		// acorda o gerente de disco (põe na fila de prontas)
	}
	// libera semáforo de acesso ao disco
	sem_up(&disk.s_disk);
	// suspende a tarefa corrente (retorna ao dispatcher)
	task_suspend(task_on, &t_suspend);
	task_switch(&t_dispatcher);
	return 0;

}


void tratador_disk()
{
	sig_flag=1;
	task_resume(&t_disk_manager);
}

pedidos_t* calc_fcfs()
{
	if(cab_disk>=disk.p_queue->block)
		count_blocks+=cab_disk-disk.p_queue->block;
	else
		count_blocks+=disk.p_queue->block-cab_disk;
	
	cab_disk=disk.p_queue->block;
	return disk.p_queue;
}

pedidos_t* calc_sstf()
{
	pedidos_t *menor,*aux;
	int aux_sum1,aux_sum2,aux_sum3;
	aux_sum1=0;
	aux_sum2=0;
	aux=disk.p_queue->next;
	menor=disk.p_queue;
	while(aux->next!=disk.p_queue)
	{
		aux_sum1=aux->block-cab_disk;
		if(aux_sum1<0)aux_sum1=aux_sum1*-1;
		aux_sum2=menor->block-cab_disk;
		if(aux_sum2<0)aux_sum2=aux_sum2*-1;
		
		if(aux_sum1<aux_sum2)
			menor=aux;
		aux=aux->next;
	}
	aux_sum1=aux->block-cab_disk;
	if(aux_sum1<0)aux_sum1=aux_sum1*-1;
	aux_sum2=menor->block-cab_disk;
	if(aux_sum2<0)aux_sum2=aux_sum2*-1;
	
	if(aux_sum1<aux_sum2)
		menor=aux;

	aux_sum3=menor->block-cab_disk;
	if(aux_sum3<0)aux_sum3=aux_sum3*-1;
	count_blocks+=aux_sum3;
	
	cab_disk=menor->block;
	return menor;
}

pedidos_t* calc_cscan()
{
	pedidos_t *proximo,*aux;
	int aux_sum1,flag_giro;
	aux_sum1=0;
	flag_giro=0;
	aux=disk.p_queue;
	proximo=NULL;
	while(aux->next!=disk.p_queue)
	{
		if(aux->block>cab_disk)
		{
			if(proximo==NULL)
				{
					proximo=aux;
					flag_giro=1;
				}
			else if(aux->block<proximo->block)
				proximo=aux;
		}
		aux=aux->next;
	}
	if(aux->block>cab_disk)
	{
		if(proximo==NULL)
			{
				proximo=aux;
				flag_giro=1;
			}
		else if(aux->block<proximo->block)
			proximo=aux;
	}

	if(flag_giro==0)
	{
		aux=disk.p_queue;
		while(aux->next!=disk.p_queue)
		{
			if(proximo==NULL)
				{
					proximo=aux;
					flag_giro=1;
				}
			else if(aux->block<proximo->block)
				proximo=aux;
			aux=aux->next;
		}
		if(proximo==NULL)
			{
				proximo=aux;
				flag_giro=1;
			}
		else if(aux->block<proximo->block)
			proximo=aux;

	}

	if(cab_disk>proximo->block)
	{
		aux_sum1= 256 - cab_disk + proximo->block;
	}
	else{
		aux_sum1= proximo->block-cab_disk;
	}
	count_blocks+=aux_sum1;
	cab_disk=proximo->block;
	return proximo;
}

pedidos_t* disk_esc()
{
	pedidos_t *escolhido;

	if( ESCALONADOR == FCFS)
		escolhido=calc_fcfs();
	else if( ESCALONADOR == SSTF)
		escolhido=calc_sstf();
	else if( ESCALONADOR == CSCAN)
		escolhido=calc_cscan();
	return escolhido;
}