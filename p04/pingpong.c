
#include "pingpong.h"
char *stack ;
int static n_task,n_abs_tasks;//Numeor de tarefas e numero de tarefas prontas 
task_t t_main,t_dispatcher,*task_on,*t_ready,*t_suspend;//Tarefa main, tarefa despachadora, ponteiro para a tarefa atual , ponteiro para fila de prontos e ponteiro para fila de suspensos.
ucontext_t c_main;//Contexto da função main

//Função que retorna a proxima tarefa a ser chamada pelo despachador, Modelo com prioridades
task_t *scheduler()
{	task_t *next,*aux;
	aux=t_ready->next;
	next=t_ready;
	while(aux!=t_ready)
	{	
		//Decide qual a tarefa com a maior prioridade dinamica
		if(next->priod>aux->priod)
			next=aux;
		aux=aux->next;

	}
	aux=t_ready;
	//Envelhece as tarefas 
	while(aux->next!=t_ready)
	{
		aux->priod=aux->priod-1;
		aux=aux->next;
	}
	aux->priod=aux->priod-1;

	next->priod=next->prio;//Retorna a prioridade dinamica para a estatica quando a tarefa é chamada


	return(next);
}
void dispatcher_body () // dispatcher é uma tarefa
{	
	task_t *next;
	while ( n_abs_tasks > 1 )
	{
		next = scheduler() ; // scheduler é uma função que decide a proxima tarefa chamada
		if (next)
		{
			task_switch (next) ; // transfere controle para a tarefa "next"
		}
	}
 task_exit(0) ; // encerra a tarefa dispatcher
}



void pingpong_init (){
//Inicia ponteiros e filas
    task_on = NULL;
    t_ready = NULL;
    t_suspend = NULL;

    setvbuf (stdout, 0, _IONBF, 0) ;
//Inicia estaticas
    n_abs_tasks=0;
    n_task=0;

//Inicia tarefa Main 
    t_main.prio=0;
    t_main.next=&t_main;
    t_main.prev=&t_main;
    t_main.tid=n_task;
    t_main.context=c_main;

//Coloca ponteiro atual na tarefa Main
    task_on=&t_main;

//Cria tarefa despachadora
    task_create(&t_dispatcher,dispatcher_body,0);

}

int task_create (task_t *task,void (*start_func)(void *),void *arg){
//Incrementa tarefa na fila de prontos e o numero de tarefas
	n_abs_tasks++;
    n_task++;

//Aloca valores de contexto e função utilizada pela tarefa
    stack = malloc (STACKSIZE) ;
	getcontext (&(task->context));
    task->context.uc_stack.ss_sp = stack ;
    task->context.uc_stack.ss_size = STACKSIZE;
    task->context.uc_stack.ss_flags = 0;
    task->context.uc_link = 0;
    task->tid=n_task;
//Aloca a prioridade padrao,0.
    task->prio=0;

//Decide por quem a tarefa sera chamada
    if(n_task==1)
    	task->parent=&t_main;
    else
    	task->parent=&t_dispatcher;
    makecontext (&(task->context), (void*)(*start_func), 1,arg);

  	if(n_task>1)
	  	queue_append ((queue_t **) &t_ready,(queue_t*) task);

    return(n_task);
}

//Troca o controle da tarefa atual para a tarefa passada como parametro
int task_switch (task_t *task) {
	ucontext_t *aux;
	aux=&(task_on->context);
	task_on=task;
    swapcontext ((aux), &(task->context)) ;
	return (task->tid);
}

//Termina a tarefa
void task_exit (int exitCode){
	n_abs_tasks--;
	if(task_on->parent==&t_dispatcher)
	queue_remove ((queue_t**) &t_ready, (queue_t*) task_on) ;
   	task_switch (task_on->parent) ;

}

//Retorna o controle para a tarefa despachadora 
void task_yield (){
	task_switch(&t_dispatcher);
}


//Retorna o id da tarefa
int task_id () {
	return(task_on->tid);
}


//Suspende a tarefa e a coloca na fila passada como parametro
void task_suspend (task_t *task, task_t **queue){
	if(queue==NULL)
		return;
	else{
		if (task==NULL)
			task=task_on;

		n_abs_tasks--;

		if(task->parent==&t_dispatcher)
		queue_remove ((queue_t**) &t_ready, (queue_t*) task) ;

	  	queue_append ((queue_t **) queue,(queue_t*) task);

	  	if(task_on==task)
	   	task_switch (task_on->parent) ;


	}
}

//Acorda a tarefa e a coloca na fila de prontos
void task_resume (task_t *task){
	queue_remove ((queue_t**) &t_suspend, (queue_t*) task);
  	queue_append ((queue_t **) &t_ready,(queue_t*) task);

}

// define a prioridade estática de uma tarefa (ou a tarefa atual)
void task_setprio (task_t *task, int prio){
	if(task==NULL)
		task=task_on;
	task->prio=prio;
	if (task->prio>20)
		task->prio=20;
	if(task->prio<-20)
		task->prio=-20;

	task->priod=task->prio;

}

// retorna a prioridade estática de uma tarefa (ou a tarefa atual)
int task_getprio (task_t *task){
	if (task==NULL)
		task=task_on;
	return(task->prio);

}
