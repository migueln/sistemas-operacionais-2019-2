
#include "pingpong.h"
char *stack ; // Vetor usado para alocar a pilha dos contextos
int static n_task; // numero de tasks
task_t t_main,*task_on;// Task do Main e task atual
ucontext_t c_main; // Contexto do main

void pingpong_init (){
    setvbuf (stdout, 0, _IONBF, 0) ;

    task_on=NULL; 

    n_task=0; // Iniciando o numero de tasks

//Definição dos parametros da task do Main
    t_main.next=&t_main; 
    t_main.prev=&t_main;
    t_main.tid=n_task;
    t_main.context=c_main;


    task_on=&t_main;// Apontando a task atual para a task Main

}

// Cria a task alocando o contexto e incrementando o numero de tasks
int task_create (task_t *task,void (*start_func)(void *),void *arg){
    n_task++;
    stack = malloc (STACKSIZE) ;
	getcontext (&(task->context));
    task->context.uc_stack.ss_sp = stack ;
    task->context.uc_stack.ss_size = STACKSIZE;
    task->context.uc_stack.ss_flags = 0;
    task->context.uc_link = 0;
    task->tid=n_task;
    makecontext (&(task->context), (void*)(*start_func), 1,arg);
  

    return(n_task);
}

//Troca o contexto e o ponteiro da task atual para a task usada como parametro
int task_switch (task_t *task) {
	ucontext_t *aux;
	aux=&(task_on->context);
	task_on=task;
    swapcontext ((aux), &(task->context)) ;
	return (task->tid);
}

//Sai da task atual atraves da função task_switch
void task_exit (int exitCode){
    task_switch (&t_main) ;
}

// retorna o id da task
int task_id () {
	return(task_on->tid);
}