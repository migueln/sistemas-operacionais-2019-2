
#include "pingpong.h"

#define TIME_Q 20//Tamanho do quantum

char *stack ;

int static n_task,n_abs_tasks,quantum; //Numero de tarefas, numero de tarefas prontas, tempo que sera atribuido a cada tarefa(de usuario) antes da interrupção

task_t t_main,t_dispatcher,*task_on,*t_ready,*t_suspend;//Tarefa main, tarefa dispachadora, ponteiro para a tarefa atual, ponteiro para fila de tarefas prontas e ponteiro para fila de suspensas 

ucontext_t c_main;//COntexto da tarefa main

struct sigaction action ;//Estrutura de sinal
struct itimerval timer;//Estrutura do timer

void tratador();// Função que trata os sinais recebidos, nesse caso "ticks"


//Retorna a proxima função chamada, Modelo de prioridade
task_t *scheduler(){	
	task_t *next,*aux;
	aux=t_ready->next;
	next=t_ready;
	while(aux!=t_ready)
	{	
		//Decide a tarefa com menor prioridade para ser chamada
		if(next->priod>aux->priod)
			next=aux;
		aux=aux->next;

	}
	aux=t_ready;
	while(aux->next!=t_ready)
	{
		//Envelhece as tarefas
		aux->priod=aux->priod-1;
		aux=aux->next;
	}
	aux->priod=aux->priod-1;

	next->priod=next->prio;


	return(next);

}
void dispatcher_body () // dispatcher é uma tarefa que decide a ordem e chama as tarefas de usuario
{	
	task_t *next;
	while ( n_abs_tasks > 1 )
	{
		next = scheduler() ; // scheduler é uma função que decidide qual sera a proxima tarefa de usuario chamada
		if (next)
		{
			task_switch (next) ; // transfere controle para a tarefa "next"
		}
	}
 task_exit(0) ; // encerra a tarefa dispatcher
}



void pingpong_init (){
    setvbuf (stdout, 0, _IONBF, 0) ;

//Iniciação das variaves estaticas
    n_task=0;
    n_abs_tasks=0;

//Iniciação dos ponteiros e filas
    t_ready = NULL;
    task_on = NULL;
    t_suspend = NULL;


//Iniciação da tarefa main
    t_main.prio=0;
    t_main.next=&t_main;
    t_main.prev=&t_main;
    t_main.tid=n_task;
    t_main.context=c_main;
    t_main.type=System;
    task_on=&t_main;

//Criacao da tarefa despachadora
    task_create(&t_dispatcher,dispatcher_body,0);
    t_dispatcher.type=System;

//Declarações para captar o sinal
	action.sa_handler = tratador ;
	sigemptyset (&action.sa_mask) ;
	action.sa_flags = 0 ;
	if (sigaction (SIGALRM, &action, 0) < 0)
	{
	perror ("Erro em sigaction: ") ;
	exit (1) ;
	}

	// ajusta valores do temporizador
	timer.it_value.tv_usec = 1000 ;      // primeiro disparo, em micro-segundos
	timer.it_value.tv_sec  = 0 ;      // primeiro disparo, em segundos
	timer.it_interval.tv_usec = 1000 ;   // disparos subsequentes, em micro-segundos
	timer.it_interval.tv_sec  = 0 ;   // disparos subsequentes, em segundos
	quantum=TIME_Q;
	// arma o temporizador ITIMER_REAL (vide man setitimer)
	if (setitimer (ITIMER_REAL, &timer, 0) < 0)
	{
	perror ("Erro em setitimer: ") ;
	exit (1) ;
	}
}

int task_create (task_t *task,void (*start_func)(void *),void *arg){
//Incrementa tarefa na fila de prontos e o numero de tarefas
	n_abs_tasks++;
    n_task++;

//Aloca valores de contexto e função utilizada pela tarefa
    stack = malloc (STACKSIZE) ;
	getcontext (&(task->context));
    task->context.uc_stack.ss_sp = stack ;
    task->context.uc_stack.ss_size = STACKSIZE;
    task->context.uc_stack.ss_flags = 0;
    task->context.uc_link = 0;
    task->tid=n_task;

//Aloca a prioridade padrao,0.
    task->prio=0;
    task->type=User;

//Decide por quem a tarefa sera chamada
    if(n_task==1)
    	task->parent=&t_main;
    else
    	task->parent=&t_dispatcher;
    makecontext (&(task->context), (void*)(*start_func), 1,arg);
  	if(n_task>1)
	  	queue_append ((queue_t **) &t_ready,(queue_t*) task);

    return(n_task);
}

//Troca o controle da tarefa atual para a tarefa passada como parametro
int task_switch (task_t *task) {
	ucontext_t *aux;
	aux=&(task_on->context);
	task_on=task;
    swapcontext ((aux), &(task->context)) ;
	return (task->tid);
}

//Termina a tarefa
void task_exit (int exitCode){
	n_abs_tasks--;
	if(task_on->parent==&t_dispatcher)
	queue_remove ((queue_t**) &t_ready, (queue_t*) task_on) ;
   	task_switch (task_on->parent) ;

}

// Reseta timer e devolve o controle para a tarefa despachadora
void task_yield (){
	quantum=TIME_Q;
	task_switch(&t_dispatcher);
}

// Retorna id da tarefa
int task_id () {
	return(task_on->tid);
}

//Suspende a tarefa e a coloca na fila passada no parametro
void task_suspend (task_t *task, task_t **queue){
	if(queue==NULL)
		return;
	else{
		if (task==NULL)
			task=task_on;

		n_abs_tasks--;

		if(task->parent==&t_dispatcher)
		queue_remove ((queue_t**) &t_ready, (queue_t*) task) ;

	  	queue_append ((queue_t **) queue,(queue_t*) task);

	  	if(task_on==task)
	   	task_switch (task_on->parent) ;


	}
}


//Acorda a tarefa passada pelo parametro e a coloca na fila de prontas
void task_resume (task_t *task){
	queue_remove ((queue_t**) &t_suspend, (queue_t*) task);
  	queue_append ((queue_t **) &t_ready,(queue_t*) task);

}

// define a prioridade estática de uma tarefa (ou a tarefa atual)
void task_setprio (task_t *task, int prio){
	if(task==NULL)
		task=task_on;
	task->prio=prio;
	if (task->prio>20)
		task->prio=20;
	if(task->prio<-20)
		task->prio=-20;

	task->priod=task->prio;

}

// retorna a prioridade estática de uma tarefa (ou a tarefa atual)
int task_getprio (task_t *task){
	if (task==NULL)
		task=task_on;
	return(task->prio);

}

//Trata o sinal recebido, Decrementando o quantum da tarefa usuario e eventualmente devolvendo o controle para a despachadora
void tratador (int signum)
{
  if (task_on->type==System)
  {
	quantum=TIME_Q;

  }
  else{
		quantum=quantum-1;
		if (quantum==0)
			task_yield();

  }
}